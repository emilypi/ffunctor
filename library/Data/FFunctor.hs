{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE Rank2Types     #-}

-- | Functor of Functors
module Data.FFunctor where

class FFunctor (f :: (* -> *) -> *) where
--ffmap :: (Functor m, Functor n) => (m ~> n) -> f m -> f n
  ffmap :: (Functor m, Functor n) => (forall a . (m a -> n a)) -> f m -> f n

-- TODO is there anything from stdlib that could have an instance?
